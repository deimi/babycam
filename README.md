# RPI Babycam

## Overview setup

```plantuml
@startuml

card "Camera" as cam
card "Microphone" as mic

node "RPi Zero" as rpi{
    component "libcamera" as libcamera
    component "ffmpeg" as ffmpeg
}

cam <-- libcamera
mic <--ffmpeg
libcamera -> ffmpeg


node "Local Proxy" as local{
    component "MediaMTX" as rtsp_local
    interface ":8554" as rtsp_local_if
}

ffmpeg -down-> rtsp_local : rtsp+auth
rtsp_local -> rtsp_local_if

node "Android" as phone{
    component "VLC" as vlc
}

node "Android" as phone2{
    component "VLC" as vlc2
}

vlc -up-> rtsp_local_if : rtsp+auth
vlc2 -up-> rtsp_local_if : rtsp+auth

@enduml
```
