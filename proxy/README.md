# Usage
- Clone Repo
- Create certificates
```bash
mkdir /etc/babycam/
cd /etc/babycam/
openssl genrsa -out server.key 2048
openssl req -new -x509 -sha256 -key server.key -out server.crt -days 3650
```
- Start proxy container
```bash
cd proxy
docker compose up -d
```
