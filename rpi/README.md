# Requirements

- RPI with camera and USB microphone
- Raspian Bookworm

```bash
sudo apt-get install ffmpeg
```
# Usage

```bash
libcamera-vid -v 0 -t 0 --framerate 20 --bitrate 3000000 --width 1280 --height 720 -o - | ffmpeg -i - -f alsa -ac 1 -itsoffset 3.5 -i plughw:1,0 -vf "transpose=1" -acodec aac -strict -2 -f rtsp rtsp://<user>:<password>@192.168.1.74:8554/babycam
```

## Install systemd service

```bash
sudo nano /etc/babycam/settings.env
sudo cp rpi/babycam.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable babycam.service
sudo systemctl start babycam.service
```
